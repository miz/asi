#!/usr/bin/perl

#
# Alternative Syno Indexer
# ASI
# release 1
#
# This script will smartly update Media Index on your Synology appliance
# You can create 'noindex' file under scan directory to skip subdir indexing
# Works on DSM 5+
# Start interactive with --repair switch 
#  if your Media Indexer stuck on "Indexing media files" forever
#
# 2015
# mizaar <at> mail ru
#
# Usage:
# 	Add to tasks or crontab
# 	perl asi.pl /volume1/video
#
#	If your Media Indexer stuck on "Indexing media files" forever:
#	perl asi.pl --repair
#
# Inspired by update-syno.sh (C) 2010 by Gerd W. Naschenweng

use File::Basename;

@include_files = ("ASF","AVI","DIVX","IMG","ISO","M1V","M2P","M2T","M2TS","M2V","M4V","MKV","MOV","MP4","MPEG4","MPE","MPG","MPG4","MTS","QT","RM","TP","TRP","TS","VOB","WMV","XVID");

# Scan directory
my $scan_dir = shift;
if (!$scan_dir) {
         $scan_dir = '/volume1/video';
         }

if ($scan_dir eq "--repair"){
    # Do the repair process
    # 
    # 0. Clean up indexer queue
    # 1. Kill all indexers
    # 2. Clean up database
    
    `rm -f /var/spool/syno_indexing_queue*`;
    `killall synoindexd`;
    `killall synoindexscand`;
    `killall synoindexworkerd`;
    `killall synomediaparserd`;
	 
	`/usr/syno/pgsql/bin/psql mediaserver admin -tA -c 'delete from video_convert'`;
	`/usr/syno/pgsql/bin/psql mediaserver admin -tA -c 'delete from video'`;
	`/usr/syno/pgsql/bin/psql mediaserver admin -tA -c 'delete from audio'`;
	`/usr/syno/pgsql/bin/psql mediaserver admin -tA -c 'delete from photo'`;
	`/usr/syno/pgsql/bin/psql mediaserver admin -tA -c 'delete from directory'`;
	`/usr/syno/pgsql/bin/psql mediaserver admin -tA -c 'delete from playlist'`;
	
    print "Done repair\n";
    exit(0);
}

#  Deal with our temporary Postgre table
`/usr/syno/pgsql/bin/psql mediaserver admin -tA -c "drop table if exists ASIFlist"`;
`/usr/syno/pgsql/bin/psql mediaserver admin -tA -c "create table ASIFlist(path varchar)"`;


# Step 0. Scan tree to find braches with 'noindex' file

@nodir = `find $scan_dir -name 'noindex'`;
$skip_dirs = "";

foreach (@nodir){
    my($fn, $dir) = fileparse($_);
    chop($dir);
    $skip_dirs .= " -path '$dir' -prune -o";
    }

# Step 1. Scan tree once again to find files with desired extensions but skipping 'noindex' branches

# You can add ' -mtime -7' to have indexed fresh content only (7 days)
@files = `find $scan_dir $skip_dirs -type f -print`;

$sql_files = "";
$cnt = 0;

foreach (@files) {
     my $file = $_;
     chomp($file);
     my $ext = ($file =~ m/([^.]+)$/)[0];
     if (grep {lc $_ eq lc $ext} @include_files) {
        #print($file,"\n");
        # We prepare SQL statement here
        $sql_files .= "('".$file."'),";
        $cnt++;
     }
     
     # Insert 10 records, not one by one, due the perfomance reason
     if ($cnt == 10) {
        chop($sql_files);
	`/usr/syno/pgsql/bin/psql mediaserver admin -tA -c "insert into ASIFlist values $sql_files;"`;
	$cnt = 0;
	$sql_files = "";
     }
     }
# Insert the last records
if ($cnt != 0){
        chop($sql_files);
	`/usr/syno/pgsql/bin/psql mediaserver admin -tA -c "insert into ASIFlist values $sql_files;"`;
	}

# Now ASIFlist table contains all reliable files

# Step 2. Remove deleted files from database
# Only records matched scanning directory will be removed

#print ("Delete: ",`/usr/syno/pgsql/bin/psql mediaserver admin -tA -c "select path from video where path not in (select path from ASIFlist) and path like '$scan_dir%'"`);
`/usr/syno/pgsql/bin/psql mediaserver admin -tA -c "delete from video where path not in (select path from ASIFlist) and path like '$scan_dir%'"`;

# Step 3. Index new files

@new_files = `/usr/syno/pgsql/bin/psql mediaserver admin -tA -c "select path from ASIFlist where path not in (select path from video)"`;

foreach (@new_files){
    #print ("Index: ",$_);
    `synoindex -a '$_'`;
    }

# Step 4. Delete empty directories records
# Only records matched scanning directory will be removed

`/usr/syno/pgsql/bin/psql mediaserver admin -tA -c "delete from directory where path not in (select distinct d.path from directory d, video v where v.path like concat(d.path,'%')) and path like '$scan_dir%'"`;

# Step 5. Add new directories

`/usr/syno/pgsql/bin/psql mediaserver admin -tA -c "insert into directory (path,title,date,mdate) select npath, RIGHT(npath, POSITION('/' in REVERSE(npath)) -1 ) as name,now(),now() from (select distinct trim(trailing '/' from (regexp_matches(path,'.+/'))[1]) as npath from asiflist) ndir where ndir.npath not in (select path from directory) and ndir.npath<>trim(trailing '/' from '$scan_dir')"`;

# Clean up database
`/usr/syno/pgsql/bin/psql mediaserver admin -tA -c "drop table if exists ASIFlist"`;